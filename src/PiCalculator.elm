module PiCalculator exposing (..)

import Arithmetic exposing (gcd)
import Html exposing (..)
import Html.Attributes exposing (property, style)
import Random exposing (Generator)
import Json.Encode exposing (string)
import Time exposing (Time, millisecond)


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( { coprimes = [], cofactors = [], pi = 0.0, rand1 = 0, rand2 = 0 }, Cmd.none )


-- model


type Rel
    = CoPrime
    | CoFactor


type alias Model =
    { coprimes : List ( Int, Int )
    , cofactors : List ( Int, Int )
    , pi : Float
    , rand1 : Int
    , rand2 : Int
    }


-- update


type Msg
    = Tick Time
    | Roll1 Int
    | Roll2 Int


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick tick ->
            ( model, Random.generate Roll1 randInt )

        Roll1 randomInt ->
            ( { model | rand1 = randomInt }, Random.generate Roll2 randInt )

        Roll2 randomInt ->
            ( { model | rand2 = randomInt } |> updateModel |> runCalculations, Cmd.none )


updateModel : Model -> Model
updateModel model =
    case isCoprime model.rand1 model.rand2 of
        CoPrime ->
            { model | coprimes = ( model.rand1, model.rand2 ) :: model.coprimes }

        CoFactor ->
            { model | cofactors = ( model.rand1, model.rand2 ) :: model.cofactors }


runCalculations : Model -> Model
runCalculations model =
    { model | pi = calculatePi (List.length model.coprimes) (List.length model.cofactors) }



--subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every (333 * millisecond) Tick



-- view


numberPair : ( Int, Int ) -> Html Msg
numberPair pair =
    let
        ( a, b ) =
            pair
    in
        li [] [ text ((toString a) ++ ", " ++ (toString b)) ]


numberList : List ( Int, Int ) -> Html Msg
numberList list =
    let
        child =
            List.map numberPair list
    in
        ul [] child


view : Model -> Html Msg
view model =
    div []
        [ h1 [ property "innerHTML" (string "&Pi; Calculator") ] []
        , h2 [] [ text ("Pi Estimate:  " ++ toString model.pi) ]
        , h2 [] [ text ("Sample Size:  " ++ toString (List.length model.cofactors + List.length model.coprimes)) ]
        , h2 [] [ text ("Error:  " ++ toString (model.pi - pi) ++ " (" ++ toString ((model.pi - pi) / pi * 100) ++ "%)") ]
        , div []
            [ div [ style [ ( "float", "left" ) ] ]
                [ h3 [] [ text ("CoPrime Pairs (" ++ toString (List.length model.coprimes) ++ ")") ]
                , numberList model.coprimes
                ]
            , div [ style [ ( "float", "left" ) ] ]
                [ h3 [] [ text ("CoFactor Pairs(" ++ toString (List.length model.cofactors) ++ ")") ]
                , numberList model.cofactors
                ]
            ]
        ]



-- misc functions


randInt : Generator Int
randInt =
    Random.int 1 100000000


calculatePi : Int -> Int -> Float
calculatePi n_coprime n_cofactor =
    sqrt (toFloat 6 / (toFloat n_coprime / toFloat (n_cofactor + n_coprime)))


isCoprime : Int -> Int -> Rel
isCoprime a b =
    if gcd a b == 1 then
        CoPrime
    else
        CoFactor
