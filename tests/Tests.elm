module Tests exposing (..)

import Test exposing (..)
import Expect
import Fuzz exposing (list, int, tuple, string)
import String

import PiCalculator exposing (..)


all : Test
all =
    describe "Miscellaneous Functions"
        [ describe "calculatePi"
            [ test "Should Come out with a value for pi" <|
                \() ->
                    Expect.equal (calculatePi 2 1) 3
            ]
        , describe "isCoprime"
            [ test "Should check if two numbers are cofactor" <|
                \() ->
                    Expect.equal (isCoprime 9 6) CoFactor
              , test "SHould check if two numhers are coprime" <|
                \() ->
                    Expect.equal (isCoprime 16 9) CoPrime
            ]
        ]
